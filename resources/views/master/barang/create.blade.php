@extends('layouts.app')
@section('title', 'Master Barang')

@section('content')

<div class="row">
    <div class="col-lg-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add Barang</h6>
            </div>
            <div class="card-body">
                <form action="" id="frmEntry" onsubmit="return false">
                    <div class="row" id="boxEntry" >
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="">Kode Barang <span class="text-danger">*</span></label>
                          <input type="text" class="form-control " id="kode_brang" readonly placeholder="Auto">
                          <div class="invalid-feedback">
                            
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="">Nama Barang <span class="text-danger">*</span></label>
                          <input type="text" class="form-control" maxlength="30" id="nama_barang">
                          <div class="invalid-feedback">
                            
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="">Stok </label>
                          <input type="number" class="form-control"   id="stock">
                          <div class="invalid-feedback">
                            
                          </div>
                        </div>
                      </div>

  
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="">Upload Foto Barang <span class="text-danger">*</span></label>
                          <!-- <input type="file" class="form-control"  required> -->
                          <div class="row">
                            <div class="col-12 text-center">
                              <label class="label-input-file" for="barang_logo">
                                <input type="file" id="barang_logo" accept="image/x-png,image/jpeg, image/png" >
                                <img src="/img/upload.png" data-default-src="/img/upload.png" width="48" class="img-input-file" alt="input-file">
                                <p class="input-file-text" data-default-label="Unggah Foto">Unggah Foto</p>
                              </label>
                              <div class="invalid-feedback">
                            
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
   
                      <div class="col-12">
                        <div class="d-flex justify-content-center">
                          <button class="btn btn-success mr-1">Save</button>
                          <a href='{{ url("barang") }}' id="btn-cancel" type="button" class="btn btn-outline-danger">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </form>
                
            </div>
        </div>
    </div>
</div>


@endsection

@section("scripts")
<script>
$(function() {

    $(".file-upload").on('dragenter', function(e) {
        $(this).parent().addClass('drag-enter')
    })
    $(".file-upload").on('dragleave drop', function() {
        $(this).parent().removeClass('drag-enter')
    })

    $("#logo_bank").on('change', function() {
        handleUploadFile('#logo_bank', {
            allowedFileType: ['jpg', 'jpeg', 'png'], // file extension (without dots)
            limitSize: 5 // in MB
        })
    })


    $('#frmEntry').submit(function(e){
        var formData = new FormData();
        $.each($('#boxEntry :input').serializeObject(), function(x, y){ 
            formData.append(x,y) 
        });
        formData.append('barang_logo',  (typeof $("#barang_logo")[0] == "undefined") ?"":$("#barang_logo")[0].files[0]);
        

        ajax({
            url : "{{ route("barang.store")}}", 
            postData : formData,
            processData: false,
            contentType: false,
            success : function(ret){
                window.location = "{{ url("barang") }}";
            }
        });
        return false;
    })
});	
</script>

@endsection