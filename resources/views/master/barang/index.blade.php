@extends('layouts.app')
@section('title', 'Master Barang')

@section('content')

    {{-- modal --}}
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="modal-add-provinsi-label" aria-hidden="true">
        <form>
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                </div>
            </div>
        </form>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Barang</h6>
            <div class="float-right">   
                         <a class="btn btn-sm btn-primary" href="{{url('barang/create')}}">Add Barang</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="bankTable" class="display table table-striped table-hover">
                  <thead>
                    <tr>
                      <th>Nama Barang</th>
                      <th>Kode Barang</th>
                      <th>Stok</th>
                      <th>Logo</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                </table>
              </div>
        </div>
    </div>

@endsection
@section('scripts')
   
    
    <script>
        $("#myModal").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-content").load(link.attr("href"));
        });

        var table = $('#bankTable').DataTable({
            dom: "<'row'<'col-sm-12'l>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            processing: true,
            serverSide: true,
            bAutoWidth: false,
            searchable: false,
            language: {
                "lengthMenu": "Tampilkan _MENU_",
                "search": "Cari: <br/>",
            },
            lengthMenu: [
                [15, 30, 50, 100, 500, 1000],
                [15, 30, 50, 100, 500, 'All']
            ],
            aaSorting: [],
            ajax: {
                url: '{{ url('barang/list') }}',
                method: 'get',
                dataType: "JSON",
                data: function(d) {
                    d.keyword = $('#input-search').val();
                    d.status = $('#select-status').val();

                }
            },
            columns: [
                {
                    data: 'nama_barang',
                    name: 'nama_barang'
                },
                {
                    data: 'kode_barang',
                    name: 'kode_barang'
                },
                
                {
                    data: 'stock',
                    name: 'stock'
                },
                {
                    data: 'img_barang',
                    name: 'img_barang'
                },

                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                }
            ],

        });

        $('#btn-search').on('click', function(e) {
            e.preventDefault();
            table.columns.adjust().draw();
        });

        $("#input-search").on('keypress', function(e){
          if(e.which == 13){
            e.preventDefault();
            table.columns.adjust().draw();
          }
        });
        
        $(document).on('click', '.btn-delete', function() {
            var id = $(this).data('id');
            swal({
                title: "Anda yakin?",
                text: "Data ini akan dihapus secara permanen",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((confirm) => {
                if (confirm) {
                    ajax({
                        url : "{{ url('barang/delete', ':id') }}".replace(':id', id),
                        postData : {_method: 'delete'},
                        success : function(ret){
                            
                            table.ajax.reload();
                        },
                    });
                }
            });
        });
    </script>

@endsection