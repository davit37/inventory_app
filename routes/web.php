<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Master\BarangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {
    Route::get("/", [DashboardController::class, "index"]);
    Route::prefix("/barang")->group(function () {
        Route::get("list", [BarangController::class, "getList"]);
    });
    Route::resource('/barang', BarangController::class);


});

require __DIR__.'/auth.php';
