<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\View\Components\Ui\SideBar;
use App\View\Components\Ui\TopBar;
use App\View\Components\Ui\Modal\ModalLogout;
use Illuminate\Support\Facades\Blade;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('side-bar', SideBar::class);
        Blade::component('top-bar', TopBar::class);
        Blade::component('modal-logout', ModalLogout::class);

    }
}
