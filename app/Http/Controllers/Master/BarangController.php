<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Barang;
use App\Library\ImageUploader;


class BarangController extends Controller
{
    public function __construct(ImageUploader $imageUploader)
    {
        $this->uploader = $imageUploader;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.barang.index');
    }

    public function getList(Request $request){

        return datatables(Barang::query())
        ->addIndexColumn()
            ->escapeColumns([])
        ->editColumn(
            'img_barang',
            function ($data) {
                return  '<img src="' .$data->img_barang. '" width="100px">';
            }
        )
        ->addColumn('action', function ($data)   {
            $html = '';

            $html .= '<a class="btn btn-icon btn-link"   href="' . route('barang.show',  $data->id) . '"> <i class="fas fa-eye"></i></a>';



            $html .= '<a class="btn btn-icon btn-link" href="' . route('barang.edit',  $data->id) . '">
            <i class="fas fa-edit"></i></a>';

            $html .= '<a class="btn btn-icon btn-link btn-delete" id="'.$data->id.'">
            <i class="fas fa-trash"></i>
          </a>';



            return $html;
        })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $filename = $this->uploader->uploadImage($data['barang_logo'], 'master/barang');

        Barang::create([
            'nama_barang'=>$data['nama_barang'],
            'kode_barang' => "BRB/".date('YmdHis'),
            'stock' => $data['stock'],
            'satuan' => "qty",
            'keterangan' => '',
            'img_barang'=>$filename,
        ]);
        return response()->json(['status' => 'success', 'msg' => 'Data berhasil ditambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Barang::find($id);
        

        return view('master.barang.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Barang::find($id);
        return view('master.barang.edit', compact('data'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Barang::find($id);
        $data->nama_barang = $request->nama_barang;
        $data->stock = $request->stock;
        $data->save();
        return response()->json(['status' => 'success', 'msg' => 'Data berhasil ditambahkan']);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
