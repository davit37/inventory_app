<?php

namespace App\Library;

use Aws\S3\S3Client;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * Undocumented class ImageUploader
 * @author
 * @package App\Library
 * @property S3Client $s3Client
 * @property string $bucket
 * @property string $path
 * @property string $url
 * @property string $filename
 * @property string $extension
 * @property string $filepath
 * @property string $filepath_thumb
 */
class ImageUploader
{
    private $awsClient;
    private $disk;

    public function __construct()
    {
        $this->disk = Storage::disk('s3');
    }
    public function uploadImage($file, $path = null)
    {

        $fileName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $fileName = sha1(time() . $fileName) . '.' . $extension;
        $filePath = $path . '/' . $fileName;
        $filePathThumb = $path . '/thumbs/' . $fileName;
        /**
         * @var $image \Intervention\Image\Image
         * compres 50%
         */

        try {

            $img = Image::make($file)->resize(1024, 1024, function ($constraint) {
                $constraint->aspectRatio();
            })->stream()->detach();

            $object = $this->disk->put($filePath, $img);

            if (!$object) {
                throw new ImageUploadFailedException;
            }

            // // Create thumbs image
            $urlImage = $this->disk->url($filePath);

            $img = Image::make($urlImage); // load image from url
            $width = 200;
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            // Upload thumbs image
            $img_thumb = $img->stream();
            $this->disk->put($filePathThumb, $img_thumb);

            $url = $this->disk->url($filePath);


            return $url;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    public function deleteImage($file)
    {
        $this->disk->delete($file);
        return true;
    }
    /**
     * upload function image format base64
     *
     * @param [type] $path file location
     * @param [type] $stringBase64
     * @return void
     */
    public function uploadBase64($stringBase64, $path)
    {

        $explodeBase64 = explode(";base64,", $stringBase64);
        $typeAuxiliary = explode("image/", $explodeBase64[0]);
        $imageType = $typeAuxiliary[1];
        $imageDecoded = base64_decode($explodeBase64[1]);
        $fileName = uniqid() . '.' . $imageType;

        try {
            // Upload Original Image

            $object = $this->disk->put($path . '/' . $fileName, $imageDecoded);

            // Create thumbs image
            $urlImage = $this->disk->url($path . '/' . $fileName);

            $img = Image::make($urlImage); // load image from url
            $width = 200;
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            // Upload thumbs image
            $img_thumb = $img->stream();
            $this->disk->put($path . '/thumbs/' . $fileName, $img_thumb);

            return $fileName;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }
    /**
     *  function upload document
     *
     * @param [binary] $file
     * @param [string] $path
     * @return file_name
     */
    public function uploadFile($file, $path = null)
    {
        $s3path = $path;

        $fileName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $fileName = sha1(time() . $fileName) . '.' . $extension;
        $filePath = $s3path . $fileName;

        try {

            $this->disk->put($filePath, fopen($file, 'r+'));

            return $fileName;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     *  function upload document, but will try upload its original name first
     *
     * @param [binary] $file
     * @param [string] $path
     * @return string
     */
    public function uploadFileOriginal($file, $path = null)
    {
        $s3path = $path;

        $fileName = str_replace(' ', '_', $file->getClientOriginalName());
        $extension = $file->getClientOriginalExtension();

        try {
            // check if the filename exists first. rename if the file is already exists
            if ($this->disk->exists($s3path . $fileName)) {
                $fileName = str_replace('.' . $extension, '', $fileName) . '-' . date('YmdHis-') . substr(sha1(rand(1, 99999) . uniqid()), 0, 10) . '.' . $extension;
            }

            $this->disk->put($s3path . $fileName, fopen($file, 'r+'));

            return $fileName;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     *  function upload document
     *
     * @param [binary] $file
     * @param [string] $path
     * @return file_name
     */
    public function uploadPdf($path, $file)
    {
        try {

            $fileName = $path . sha1(time() . $path) . ".pdf";

            $this->disk->put($fileName, $file);

            $url = $this->disk->url($fileName);

            return $url;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
