<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Barang extends Model
{
    use SoftDeletes;
    protected $table = 'mst_barang';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'nama_barang',
        'kode_barang',
        'stock',
        'barang_logo',
        'satuan',
        'img_barang',
        'keterangan',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
}
